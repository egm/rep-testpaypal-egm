﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="~/STD/Paypal_Sample_2.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    
 Gestione  Carrello - Con 1 Riga:
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="paypal">

    <input type="hidden" name="cmd" value="_cart"/>
    <input type="hidden" name="add" value="1"/>

    <input type="hidden" name="business" value="youremailaddress@yourdomain.com" />
    <input type="hidden" name="item_name" value="My Cart Item 1" />

    <input type="hidden" name="amount" value="10.00" />
    <input type="hidden" name="shopping_url" 
           value="http://www.yourwebsite.com/shoppingpage.html" />

    <input type="hidden" name="return" value="http://www.yourwebsite.com/success.html" />
    <input type="hidden" name="cancel_return" value="http://www.yourwebsite.com/cancel.html" />

    <input type="hidden" name="bn" value="PP-ShopCartBF:x-click-but22.gif:NonHosted" />
    <input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but22.gif" border="0"

        name="submit" alt="Make payments with PayPal - it's fast, free and secure!" />
    <img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1" />
</form>

 Gestione Carrello - Con 2 Riga:
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="paypal" >

    <input type="hidden" name="cmd" value="_cart" />
    <input type="hidden" name="add" value="1" />

    <input type="hidden" name="business" value="youremailaddress@yourdomain.com" />
    <input type="hidden" name="item_name" value="My Cart Item 2" />

    <input type="hidden" name="amount" value="5.00" />
    <input type="hidden" name="shipping" value="1.00" />

    <input type="hidden" name="shopping_url" 
        value="http://www.yourwebsite.com/shoppingpage.html" />
    <input type="hidden" name="return" value="http://www.yourwebsite.com/success.html" />

    <input type="hidden" name="cancel_return" value="http://www.yourwebsite.com/cancel.html" />
    <input type="hidden" name="bn" value="PP-ShopCartBF:x-click-but22.gif:NonHosted" />

    <input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but22.gif" border="0"
        name="submit" alt="Make payments with PayPal - it's fast, free and secure!" />

    <img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1" />
</form>

Pagamenti Abbonamenti
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_xclick-subscriptions"/> 
    <input type="hidden" name="business" value="youremailaddress@yourdomain.com"/> 
    <input type="hidden" name="item_name" value="Something"/> 
    <input type="submit" value="Abbonati" /> 

    <%-- Tutte le varibili indicate sono spiegate in dettaglio al link --%>
    <%--https://developer.paypal.com/docs/paypal-payments-standard/integration-guide/Appx-websitestandard-htmlvariables/--%>
    <input type="hidden" name="a1" value="0"/> 
    <input type="hidden" name="p1" value="3"/> 
    <input type="hidden" name="t1" value="D"/> 
    <input type="hidden" name="a3" value="10.00"/> 
    <input type="hidden" name="p3" value="1"/> 
    <input type="hidden" name="t3" value="M"/> 
    <input type="hidden" name="src" value="1"/> 
    <input type="hidden" name="srt" value="0"/> 
    <input type="hidden" name="sra" value="1"/> 

</form>

Visualizza Carrello:
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="paypal" >
    <input type="hidden" name="cmd" value="_cart" />

    <input type="hidden" name="display" value="1" />
    <input type="hidden" name="business" value="youremailaddress@yourdomain.com" />

    <input type="hidden" name="shopping_url" 
        value="http://www.yourwebsite.com/shoppingpage.html" />
    <input type="image" src="https://www.paypal.com/en_US/i/btn/view_cart_02.gif" 
           name="submit" alt="Make payments with PayPal - it's fast, free and secure!" />

</form>

</body>
</html>
