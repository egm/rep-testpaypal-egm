﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="~/STD/Paypal_Sample_1.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    
    <%-- Gestione Invio Semplice importo a Paypal ad account Cliente senza spese di spedizione --%>
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

        <input type="hidden" name="cmd" value="_xclick" />
        <input type="hidden" name="business" value="michele.derrico@egmsistemi.it" />

        <input type="hidden" name="item_name" value="My painting" />
        <input type="hidden" name="amount" value="10.00" /> 
        <input type="submit" value="Acquista" />

    </form>

    <%-- Gestione Invio Semplice importo a Paypal ad account Cliente con spese di spedizione --%>
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

        <input type="hidden" name="cmd" value="_xclick" />
        <input type="hidden" name="business" value="youremailaddress@yourdomain.com" />

        <input type="hidden" name="item_name" value="My painting" />
        <input type="hidden" name="amount" value="10.00" />

        <input type="hidden" name="shipping" value="3.00" /> 
        <input type="hidden" name="handling" value="2.00" />

        <input type="submit" value="Acquista con Parametri" />
    </form>

</body>
</html>
