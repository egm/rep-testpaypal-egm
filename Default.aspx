﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TestPayPal._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1><img src="Img/PayPal.png" width="100px" height="100px"/></h1>
        <p class="lead">Questo progetto ha come obiettivo quello di analizzare le funzionalità standard di interfacciamento 
                        con PAYPAL.</p>
        <p><a href="https://developer.paypal.com/home" class="btn btn-primary btn-lg">Documentazione On-Line &raquo;</a></p>
    </div>

    <div class="row">

        <div class="col-md-4">
            <h4>(STD) - Standard</h4>
            <p>
                NuGet is a free Visual Studio extension that makes it easy to add, remove, and update libraries and tools in Visual Studio projects.
            </p>
            <p>
                <a class="btn btn-default" href="STD/Paypal_Sample_1.aspx"> Esempio 1 &raquo;</a>
                <a class="btn btn-default" href="STD/Paypal_Sample_2.aspx"> Esempio 2 &raquo;</a>
            </p>
        </div>

        <div class="col-md-4">
            <h4>(PDT) - Payment Data Transfer</h4>
            <p>
                Questo progetto ha come obiettivo quello di analizzare le funzionalità standard di interfacciamento 
                con PAYPAL Payment Data Transfer.
            </p>
            <p>
                <a class="btn btn-default" href="PDT/PDTStart.aspx"> Esempio &raquo;</a>
            </p>
        </div>

        <div class="col-md-4">
            <h4>(IPN) - Instant Payment Notification</h4>
            <p>
                Con IPN, PayPal invia un post HTTP a un URL specificato ogni volta che si verifica una nuova transazione o lo stato di
                una transazione precedente viene aggiornata. Ciò consente di aggiornare automaticamente un database con
                i dettagli della transazione, inviare un messaggio e-mail di conferma dell'ordine personalizzato all'acquirente, o
                eseguire altri processi automatizzati simili.
            </p>            
        </div>
    </div>

</asp:Content>
