﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PDTStart.aspx.cs" Inherits="LepiPele.PayPal.PDT.PDTStart" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> Payment Data Transfer (PDT) </title>
</head>
<body>

    <h2>Payment Data Transfer (PDT)</h2>
      <%-- Tutte le varibili indicate sono spiegate in dettaglio al link --%>
      <%--https://developer.paypal.com/docs/paypal-payments-standard/integration-guide/Appx-websitestandard-htmlvariables/--%>
    <form action="<%= ConfigurationManager.AppSettings["PayPalSubmitUrl"] %>" method="post">
        <input type="hidden" name="cmd" value="_xclick" />        
        <input type="hidden" name="business" value="<%= ConfigurationManager.AppSettings["PayPalUsername"] %>" />  <%--PayPalUsername--%>
        <input type="hidden" name="tx" value="XXXXXX00199"/>  <%--Transaction ID --%>
        <input type="hidden" name="item_name" value="Ordine n° - XXXXXX00199" /> <%--Riferimenti dell'Ordine --%>

        <input type="hidden" name="currency_code" value="EUR" /> <%--Valuta--%>
        <input type="hidden" name="amount" value="10.00" /> <%--Importo--%>
        <input type="hidden" name="shipping" value="5.00" /> <%--Spese di Spedizione--%>

        <input type="hidden" name="image_url" value="https://www.egmsistemisrl.it/images/logo_EGM.png" />

        <%-- E' necessario che il n° fattura cambi ad ogni ordine in quanto Paypal verifica la presenza della fattura --%>
        <%--<input type="hidden" name="invoice" value="Fattura n°: AR0099919" />--%>

        <input type="hidden" name="return" value="https://localhost:44326/PDT/PDTSuccess.aspx" />

        <%-- Utilizzata come parametro di tracciabilità personalizzato --%>
        <input type="hidden" name="custom" value="Registrazione Iniziata: <%= DateTime.Now.ToString() %>" />
        <input type="submit" value="Paga" />
    </form>
    <div>
        <br />
        <b>Utilizza le seguenti credenziali per pagare:</b><br />
        <b>User: </b>attilio.pregnolato@egmsistemi.it<br />
        <b>Password:</b>Egmsistemi14
    </div>
</body>
</html>
